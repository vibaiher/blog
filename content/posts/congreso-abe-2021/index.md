---
title: "Congreso ABE 2021"
date: 2021-11-08T19:09:50+01:00
useRelativeCover: true
cover: "images/ito-alberto-cover.jpg"
description: He pasado el último fin de semana con la gente de la Escuela Bonsai Valencia en el Congreso ABE 2021 en Fuenlabrada. Este es mi resumen en fotos.
tags: ["bonsais", "exposiciones"]
---

He pasado el último fin de semana con la gente de la [Escuela Bonsai Valencia](http://escuelabonsaivalencia.es/) en el [Congreso ABE 2021](http://www.abebonsai.es/) en Fuenlabrada.

Todos los arboles expuestos eran de mucha calidad, las demostraciones y charlas fueron muy interesantes y el mercadillo me encantó, de hecho me lleve dos arbolitos para Valencia 😜.

Hice algunas fotos con la cámara del móvil, que no es de muy buena calidad, pero ahí van:

## La composición de shohines de Alberto Gimeno

{{< figureproc "images/composicion-alberto.jpg" Resize "760x" center "Pinus thunbergii, Nioi, prunus, eleagnus" >}}

{{< figureproc "images/pinus-thunbergii-alberto-1.jpg" Resize "760x" center "Pinus thunbergii, premio a la mejor conífera shohin" >}}

{{< figureproc "images/pinus-thunbergii-alberto.jpg" Resize "760x" center "Vista desde abajo de la ramificación" >}}

{{< figureproc "images/composicion-alberto-1.jpg" Resize "760x" center "Rosal japones" >}}

{{< figureproc "images/composicion-alberto-2.jpg" Resize "760x" center "Nioi" >}}

{{< figureproc "images/composicion-alberto-eleagnus-calderon.jpg" Resize "760x" center "El eleagnus de Calderón" >}}

## La composición de shohines de Daviz Ruiz

{{< figureproc "images/composicion-daviz-ruiz.jpg" Resize "760x" center "Pinus thunbergii, eleagnus, ¿?, zelkova nire, ito" >}}

{{< figureproc "images/zelkova-nire-daviz-ruiz.jpg" Resize "760x" center "Zelkova nire" >}}

## El Eleagnus de Pedro

{{< figureproc "images/eleagnus-pedro.jpg" Resize "760x" center "Se llevo el premio a Valencia también" >}}

## El Olmo de Calderón

{{< figureproc "images/olmo-calderon.jpg" Resize "760x" center "Este olmo se merecia algún premio..." >}}

## Los mirtos de la exposición

{{< figureproc "images/mirto-calderon.jpg" Resize "760x" center "El de Calderón" >}}

{{< figureproc "images/mirto-juande.jpg" Resize "760x" center "El mirto de Juande" >}}

{{< figureproc "images/mirto.jpg" Resize "760x" center "Otro mirto" >}}

## Los tejos de la exposición

{{< figureproc "images/taxus-baccata.jpg" Resize "760x" center "Taxus baccata, ganó el premio al mejor arbol de la exposición" >}}

{{< figureproc "images/taxus-baccata-demo.jpg" Resize "760x" center "Otro taxus baccata, se formó en una demostración de otro congreso" >}}

{{< figureproc "images/taxus-cuspidata.jpg" Resize "760x" center "Taxus cuspidata. Un tejo japones pequeño" >}}

{{< figureproc "images/taxus-cuspidata-1.jpg" Resize "760x" center "La vena viva y la madera muerta del tejo japones" >}}

## Algunos granados de la exposición

{{< figureproc "images/nejikan.jpg" Resize "760x" center "Nejikan. El granado japones" >}}

{{< figureproc "images/granado.jpg" Resize "760x" center "Granado autóctono" >}}

{{< figureproc "images/granado-detalle.jpg" Resize "760x" center "Detalle del granado autóctono" >}}

## Juniperus chinensis itoigawa de Alberto Gimeno

{{< figureproc "images/ito-alberto.jpg" Resize "760x" center "Mejor conífera de tamaño Chuhin" >}}

## Plantaciones de juniperos sobre roca

{{< figureproc "images/ito-roca.jpg" Resize "760x" center "Ito sobre roca" >}}

{{< figureproc "images/segundo-ito-roca.jpg" Resize "760x" center "Otro ito sobre roca" >}}

{{< figureproc "images/tercer-ito-roca.jpg" Resize "760x" center "Otro ito más sobre roca" >}}

## Los arces, bien coloridos

{{< figureproc "images/arce-buergerianum.jpg" Resize "760x" center "Arce buergerianum" >}}

{{< figureproc "images/arce-buergerianum-daihin.jpg" Resize "760x" center "Otro arce buergerianum, más grande" >}}

{{< figureproc "images/arce-palmatum.jpg" Resize "760x" center "Arce palmatum, bien cargadito de rojo y una nebari impresionante" >}}

{{< figureproc "images/arce-palmatum-1.jpg" Resize "760x" center "Arce palmatum, multicolor, no le caben más hojas" >}}

## Pinos espectaculares

{{< figureproc "images/pinus.jpg" Resize "760x" center "Espectacular, aunque no sé que variedad es..." >}}

{{< figureproc "images/pinus-thunbergii-shohin.jpg" Resize "760x" center "Pino negro japones" >}}

{{< figureproc "images/pinus-thunbergii-daihin.jpg" Resize "760x" center "Otro pino negro japones" >}}

{{< figureproc "images/pinus-sylvestris.jpg" Resize "760x" center "Un sylvestris" >}}

{{< figureproc "images/pinus-2.jpg" Resize "760x" center "Otro que no sé que variedad es..." >}}

## Y muchos más...

{{< figureproc "images/carpe-coreano.jpg" Resize "760x" center "Carpe coreano" >}}

{{< figureproc "images/composicion-shohin.jpg" Resize "760x" center "Thunbergii, prunus, gardenia, ilex, eleagnus" >}}

{{< figureproc "images/otra-composicion-shohin.jpg" Resize "760x" center "Taxus, berberis, ¿?" >}}

{{< figureproc "images/otra-nire.jpg" Resize "760x" center "Una nire pequeña, una joya" >}}

{{< figureproc "images/hiedra.jpg" Resize "760x" center "Una hiedra formada como bonsai, 😱" >}}

{{< figureproc "images/malus-sp.jpg" Resize "760x" center "Un manzano, ganador del premio al bonsai autóctono" >}}

{{< figureproc "images/sabina.jpg" Resize "760x" center "Una sabina" >}}

{{< figureproc "images/otra-sabina.jpg" Resize "760x" center "Una madera preciosa" >}}

Si encuentro más fotos iré añadiendolas, faltan muchos arboles todavía!
