---
title: "Clase de diciembre 2021"
date: 2021-12-20T16:21:50+01:00
useRelativeCover: true
cover: "images/mirto-despues-detalle-3.jpg"
description: El viernes tuve otra vez clase y estuve trabajando un par de tocones de madera muerta, además de una mini zelkova nire.
tags: ["bonsais", "clases", "mirto-tarantina", "zelkova-nire", "olmo-chino"]
---

El viernes 10 de diciembre volví a tener clase de [la escuela](http://escuelabonsaivalencia.es/contacto/). Hacía poco tiempo que había tenido la anterior clase y con la bajada de temperaturas los árboles no están muy activos, así que no sabía si tenía árboles que podía trabajar... Calderón me dijo que la madera muerta se podía trabajar en cualquier momento del año, así que me traje un par de árboles a los que llevaba tiempo queriendo rebajar los tocones que tenían, además de tres plantoncitos que estoy engordando y que quería revisar con los profesores.

## El mirto tarantina

Desde que tengo este mirto tiene un tocón bastante grande por donde lo cortaron.

{{< figureproc "images/mirto-antes.jpg" Resize "1920x" center >}}
{{< figureproc "images/mirto-antes-1.jpg" Resize "1920x" center >}}

Calderón me pidió que usará las gubias para ir marcando la vena viva y muerta alrededor del tocón de madera. Después cogió un martillo y una gubia, y a mano fue poco a poco quitando el tocón y dejando la madera lo más natural que se pudo... Parece mentira que solo quitando el tocón, gane tanta conicidad.

{{< figureproc "images/mirto-despues-frente.jpg" Resize "1920x" center >}}
{{< figureproc "images/mirto-despues-detalle.jpg" Resize "1920x" center >}}
{{< figureproc "images/mirto-despues-detalle-1.jpg" Resize "1920x" center >}}
{{< figureproc "images/mirto-despues-detalle-2.jpg" Resize "1920x" center >}}

{{< figureproc "images/mirto-despues-detalle-3.jpg" Resize "1920x" center >}}

## El olmo chino

Este Olmo chino lo compró Elena una vez que fuimos a dar una vuelta por Centro Bonsái en Alboraya. A penas ha pasado por clase, siempre lo he trabajado yo solo en casa. Y se nota 😜.

{{< figureproc "images/olmo-antes.jpg" Resize "1920x" center >}}
{{< figureproc "images/olmo-antes-1.jpg" Resize "1920x" center >}}

Al igual que el mirto, con las gubias fui buscando la vena viva, en este caso era bastante más sencillo. Como ya había perdido muchas hojas, y las que quedaban me molestaban para meterle mano, lo acabé defoliando. Después con una Dremmel que me prestó Juande y con un par de brocas distintas, Calderón redujo al máximo el tocón. Probablemente, nunca cierre del todo la herida, pero ya no se ve y cambia mucho la visión que tenía de este árbol.

{{< figureproc "images/olmo-despues.jpg" Resize "1920x" center >}}
{{< figureproc "images/olmo-despues-detalle.jpg" Resize "1920x" center >}}
{{< figureproc "images/olmo-despues-detalle-1.jpg" Resize "1920x" center >}}

Cómo nota adicional, Calderón sugirió acodarlo bastante arriba para reducirle bastante el tamaño y hacer una escoba con un tronco cortito.

## El plantoncito de zelkova nire

Este plantoncito me lo dio Pasky que trajo un montón para que el que quisiera se llevará alguno.

{{< figureproc "images/zelkova-antes-trasplante-2020.jpg" Resize "1920x" center "Antes del trasplante a principios de año" >}}
{{< figureproc "images/zelkova-antes.jpg" Resize "1920x" center "Hace unos meses cuando me instaló Calderón el sistema de riego" >}}

Lo traje a clase principalmente por ver si tenía que cortar ya la rama de sacrificio y seleccionar la siguiente, pero como le dije a Calderón que me gustaría hacer algún árbol pequeñito, cuando me descuidé ya no quedaba árbol... 🤣🤣

{{< figureproc "images/zelkova-despues.jpg" Resize "1920x" center >}}
{{< figureproc "images/zelkova-despues-1.jpg" Resize "1920x" center >}}
{{< figureproc "images/zelkova-despues-2.jpg" Resize "1920x" center >}}

El resto de árboles que traje se quedaron tal cual estaban, en cuanto pueda los pasaré a una maceta más grande, y a seguir engordando...
