---
title: "Clase de noviembre 2021"
date: 2021-12-16T22:41:32+01:00
useRelativeCover: true
cover: "images/eleagnus-nuevo-frente.jpg"
description: El 19 de noviembre tuve clase de bonsái en la escuela y como casi siempre, estuve trabajando con Calderón. Traje a clase todos los árboles que tengo más avanzados, además de los dos árboles que compre en el congreso de ABE.
tags: ["bonsais", "clases", "mirto", "mirto-tarantina", "eleagnus", "tejo-japones", "zelkova-abe", "mirto-abe"]
---

El 19 de noviembre tuve clase de bonsái en [la escuela](http://escuelabonsaivalencia.es/contacto/) y como casi siempre, estuve trabajando con Calderón. Traje a clase todos los árboles que tengo más avanzados, además de los dos árboles que compre en el congreso de ABE.

## El mirto

Este árbol lo tengo ya desde hace un par de años. Sobrevivió conmigo la pandemia... y digo sobrevivió porque el verano de antes casi no lo cuenta, pero resucitó. A pesar de estar ya en noviembre está brotando por todos lados. En la foto es el que aparece más a la derecha:

{{< figureproc "images/mirtos-taxus-antes.jpg" Resize "760x" center >}}

Calderón estuvo pegando un vistazo y únicamente corrigió un par de ramas y recortó algunas otras. Tiene un par de ramas que no entran en el diseño, pero que de momento estamos dejando a su aire porque nos ayudan a aumentar la vena viva en una zona del tronco que se secó hace un par de veranos... A pesar de eso, va como un tiro y creemos que va a salir un bonsái muy chulo, con un tronco muy interesante, como azotado por el viento:

{{< figureproc "images/mirto-despues-frente.jpg" Resize "760x" center >}}

{{< figureproc "images/mirto-despues-posterior.jpg" Resize "760x" center >}}

## El eleagnus

Este árbol lo compré antes de verano a Jaume Canals. Ya ha pasado varias veces por la escuela, y después de escuchar las opiniones de casi todos los profes, tenía claro que quería cambiar el frente del árbol. 

Tal y como me indicó Pedro, después de la clase del mes anterior le escribí una **T** en la maceta para indicar el frente y el ángulo de plantado que estuvimos hablando en la clase de septiembre.

{{< figureproc "images/eleagnus-antes.jpg" Resize "760x" center "Así estaba la mañana de antes de la clase" >}}

Una vez en clase, estuve hablando con Alberto, Calderón y Pedro sobre que frente preferían, y también con más alumnos que se acercaron... Al final continué con la idea que llevaba que era la que me había dicho Calderón inicialmente, ya que era el frente donde el tronco tenía más movimiento, mejor conicidad, la base más ancha y las ramas principales da la impresión de que nacen escalonadas y no todas desde el mismo punto. Además, con este frente evitamos ver las cicatrices que le van a quedar de los grandes cortes que tiene.

Una vez decidido el frente, me puse manos a la obra, con la ayuda de Pedro. La rama más baja de la izquierda ahora pasa a ser una rama trasera, y para ello hay que podar una gran parte de ella, las otras dos ramas siguientes de la izquierda hay que bajarlas con tensores para ayudar a reducir el hueco que queda ahora, y por último, la rama más baja de la derecha le tenemos que reducir el tamaño, para que no se fuera muy lejos del centro del árbol.

Así es como luce ahora:

{{< figureproc "images/eleagnus-nuevo-frente.jpg" Resize "760x" center >}}

{{< figureproc "images/eleagnus-variante-nuevo-frente.jpg" Resize "760x" center >}}

## El mirto tarantina

Este es el primer árbol que compré siguiendo los consejos de los profes de la escuela. Probablemente, no tenga la calidad de los otros, pero lleva conmigo desde que empecé con esto, y es el que más he trabajado y el que más ha evolucionado. Es el que aparece en medio de los tres:

{{< figureproc "images/mirtos-taxus-antes.jpg" Resize "760x" center >}}

Si antes decía que el otro mirto iba como un tiro y que continuaba brotando aún con el frío, este no iba a ser menos... En esta clase acortamos casi todas las ramas para aprovechar que todavía está brotando y que lo está haciendo cerca del tronco, así podremos conseguir ramas muy ramificadas y tener muchos brotes nuevos que poder seleccionar más adelante.

También le quitamos una de las ramas más bajas que tenía que crecer en el interior de una curva y que ya no necesitábamos. Aprovechando que podamos esa rama, dejamos un toconcito para poder colocar un tensor y bajar un poco la rama que tenía arriba para que no salga tan recta.

{{< figureproc "images/mirto-tarantina-despues.jpg" Resize "760x" center >}}

## El tejo japones

Este llegó poco después del mirto anterior, así que también le tengo mucho cariño. Es una especie que va más lenta que el resto, o al menos de la manera en que yo lo cultivo y en las condiciones climáticas que lo tengo. Es el primero que se ve por la izquierda:

{{< figureproc "images/mirtos-taxus-antes.jpg" Resize "760x" center >}}

Como se observa por la rama de sacrificio, todavía tiene bastante que engordar. No había que hacerle mucho, así que simplemente lo alambre un poco, para practicar...

{{< figureproc "images/taxus-despues-1.jpg" Resize "760x" center >}}

{{< figureproc "images/taxus-despues-2.jpg" Resize "760x" center >}}

## El mercadillo del congreso de ABE: Zelkova

Cuando fui al mercadillo iba con la intención de buscar algún olmo de los que hacen mucha corteza, [como el de Calderón](/posts/congreso-abe-2021#el-olmo-de-calderón). No encontré mucha variedad por el precio que buscaba, pero al final me quede con este:

{{< figureproc "images/zelkova-nire-abe.jpg" Resize "760x" center >}}

Lo trasplantaremos dentro de unos meses, y veremos si le hacemos un acodo o si en el trasplante directamente cortamos y nos quedamos solo con la primera rama, y el resto intentamos esquejarlo.

## El mercadillo del congreso de ABE: Otro mirto

El otro árbol que me traje fue otro mirto, mucho más grande que el resto que tengo, con mucha madera muerta. Me costó decidirme a comprarlo, pero todos los profes me dijeron que podía salir algo muy bonito de este ejemplar:

{{< figureproc "images/mirto-abe-antes.jpg" Resize "760x" center >}}

Lo llevé a clase para ver si teníamos que hacerle algo, aunque fuera pensar un diseño inicial, y así fue, es demasiado pronto para tocarlo mucho, tiene que coger vigor, y brotar por muchos sitios para ir viendo por donde meterle mano.

De momento Calderón le podó un poco la parte de más arriba, principalmente para que pueda transportarlo más fácilmente. Es muy probable que nada del verde de arriba sirva, ya que la idea es dejar toda esa parte con madera muerta, algo bastante difícil de ver en este tipo de árboles.

{{< figureproc "images/mirto-abe-despues-frente.jpg" Resize "760x" center >}}

{{< figureproc "images/mirto-abe-despues-posterior.jpg" Resize "760x" center >}}
